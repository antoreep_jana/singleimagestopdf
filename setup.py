from setuptools import setup, Extension

with open('README.md') as f:
	readme = f.read()

setup(
	name = 'singleImagesToPDF',         # How you named your package folder (MyLib)
	packages = ['singleImagesToPDF'],   # Chose the same as "name"
	version = '4.2.1',      # Start with a small number and increase it with every change you make
	license='MIT',        # Chose a license from here: https://help.github.com/articles/licensing-a-repository
	description = 'It helps you convert all the images in the current working directory (os.getcwd()) to a single pdf file',   # Give a short description about your library
	long_description = readme,
	long_description_content_type = 'text/markdown',
	author = 'Antoreep Jana',                   # Type in your name
	author_email = 'antoreepjana@gmail.com',      # Type in your E-Mail
	url = 'https://gitlab.com/antoreep_jana/singleimagestopdf/tree/master',   # Provide either the link to your github or to your website
	download_url = 'https://gitlab.com/antoreep_jana/singleimagestopdf/-/archive/v4.2/singleimagestopdf-v4.2.tar.gz',    # I explain this later on
	keywords = ['Image to PDF', "Merge Images", "pdf", "PDF", "PDF maker from images", "jpeg to pdf", "Convert Images to PDF"],   # Keywords that define your package best
    package_data = {'singleImagesToPDF' : ['salt', 'string1', 'token1']},
    include_package_data = True,
	#install_requires=[            # I get to this in a second
	#       
	#    ],
	classifiers=[
	'Development Status :: 5 - Stable',      # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
	'Intended Audience :: Developers',      # Define that your audience are developers
	'Topic :: Software Development :: Build Tools',
	'License :: OSI Approved :: MIT License',   # Again, pick a license
	'Programming Language :: Python :: 3',      #Specify which pyhton versions that you want to support
	'Programming Language :: Python :: 3.4',
	'Programming Language :: Python :: 3.5',
	'Programming Language :: Python :: 3.6',
	],
)
