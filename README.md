# singleImagesToPDF

This helps you to convert all the image files in your current working directory (os.getcwd()) to pdf files and merge them (if you want) to be combined in a PDF file format.


Specifications:
======
Supported File Formats :- JPEG/JPG/PNG<br/>
Supported Operating Systems :- Linux/Windows<br/>
Programming Language :- Python3<br/>

Using this you can convert all your images gathered (either notes or documents) to a single pdf file. 

How to Use?
======
All you need to do is open your command promt in the required directory(folder) and run:<br/>
    1) **pip install singleImagesToPDF**<br/>
    2) make a python script and add the following lines of code<br/>
       &nbsp;&nbsp;&nbsp;&nbsp;a) Do the necessary **import**:-<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; **from singleImagesToPDF.pdfFromMultiImages import pdfFromMultiImgs**<br/>
       &nbsp;&nbsp;&nbsp;&nbsp; b) Make an instance and call the merge function<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**a = pdfFromMultiImgs()**<br/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**a.merge()**

Follow along the instructions and your images (usually notes/documents) will be merged into a single pdf or keep the single pdfs corresponding to the images as required.

Image Format supported :- JPG/JPEG/PNG

Platform Supported :- Linux/Windows

Use Cases :- <br/>
======
   &nbsp;&nbsp;&nbsp;&nbsp; a) Can be used for merging the images obtained through whatsapp in your computer<br/>
   &nbsp;&nbsp;&nbsp;&nbsp; b) Can be used to merge the images obtained from mail,<br/>
    or any other place where you can utilize it for

